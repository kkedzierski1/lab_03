using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using Lab_03.Models;
using Lab_03.Context;


namespace lab_03
{
    public static class PeopleHttpAdd
    {
        [FunctionName("PeopleHttpAdd")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "person")] HttpRequest req,
            ILogger log)
        {
           
            log.LogInformation("Request new person to database");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                //var person = req.Content.ReadAsAsync<Person>();
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                Person person = JsonConvert.DeserializeObject<Person>(requestBody);              
                     var db = new ContextDb(connectionString);
                     db.AddPerson(person);
                     return new OkObjectResult(person);             
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        
        }
    }
}
