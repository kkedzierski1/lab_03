﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_03.Models
{
    class Person
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
