﻿using Lab_03.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Lab_03.Context
{
    class ContextDb
    {
        private readonly string connectionString;
        private const string Query = "Select * from people";

        public ContextDb(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<Person> GetPeople()
        {
            var people = new List<Person>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(Query, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    people.Add(new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString()

                    });
                }
                reader.Close();
            }

            return people;
        }

        public void AddPerson([FromBody] Person person)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryAddPerson = $"INSERT INTO PeopleDb (FirstName, LastName, PhoneNumber) VALUES ('{person.FirstName}','{person.LastName}','{person.PhoneNumber}');";
                SqlCommand command = new SqlCommand(queryAddPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                reader.Close();
            }
        }

        public ActionResult DeletePerson(int person_Id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryAddPerson = $"DELETE from PeopleDb where id = {person_Id};";
                SqlCommand command = new SqlCommand(queryAddPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                reader.Close();
            }
            return new OkResult();
        }

        public Person GetPerson(int person_Id)
        {
            Person person = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryGetPerson = $"SELECT * FROM PeopleDb WHERE PersonId = {person_Id};";
                SqlCommand command = new SqlCommand(queryGetPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    person = new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        PhoneNumber = reader["PhoneNumber"].ToString()
                        
                    };
                }
                reader.Close();
            }
            return person;
        }


    }
}
