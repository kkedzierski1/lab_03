using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Lab_03.Models;
using Lab_03.Context;

namespace lab03
{
    public static class PeopleHttpDelete
    {
        [FunctionName("PeopleHttpDelete")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "people/{id}")] HttpRequest req,
            ILogger log, int person_id)
        {
            log.LogInformation("Delete peron from database");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                var db = new ContextDb(connectionString);
                db.DeletePerson(person_id);
                return new OkResult();
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }

        }
    }
}
